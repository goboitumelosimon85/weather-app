package com.goboitumelo.weatherapp.model;

public interface WeatherForecastResultHandler {
    void processResources(CompleteWeatherForecast completeWeatherForecast, long lastUpdate);
    void processError(Exception e);
}
