package com.goboitumelo.weatherapp.service;

public interface WifiScanCallback {
    void onWifiResultsAvailable();
}